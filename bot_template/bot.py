# -*- coding: utf-8 -*-
import telebot
import random
import requests
import re

from datetime import datetime, date

from os import listdir
from os.path import isfile, join

from telebot import types

from config import *
from apps import gilbert
from apps.Weather import Weather
from apps.Logger import Logger




# only used for log recived messages
def logger(messages):
    for m in messages:
        commands_log.write_debug(m.text)
        #save all data needed in the log file
        #break

#SETUP BOT
bot = telebot.TeleBot(TOKEN)
bot.set_update_listener(logger)

botimg = os.path.join(MEDIA_PATH, 'img')

# Instance Weather
weather = Weather(TOKEN_WEATHER)

#Instance for Errors
#errors_log = Logger("ERROR","error.log","./")

#Instance for commands
commands_log = Logger("DEBUG","commands.txt","./")

#Instance for commands

@bot.message_handler(commands=['github', 'git', 'bitbucket'])
def command_github(message):
    github_url = 'https://bitbucket.org/aleixmartinez/bot_template'
    bot.send_message( message.chat.id, github_url)


@bot.message_handler(commands=['whoami'])
def command_whoami(message):
    user = message.from_user
    response = 'IdUser:%d \nName: %s\nUsername: %s\n' % (
                    user.id,
                    user.first_name,
                    user.username if user.username else ''
                )
    bot.send_message(message.chat.id, response)

@bot.message_handler(commands=['whoareu'])
def command_whoareu(message):
    response = "What do you think?\nI'm just a Bot."
    bot.send_message(message.chat.id, response)

@bot.message_handler(commands=["gilbert"])
def command_gilbert(message):
    try:
        date = gilbert.get_image()
        bot.send_message(message.chat.id, "Gilbert: " +date)
        photo = open(os.path.join(botimg,"gilbert","gilbert.jpg"), "rb")
        bot.send_photo(message.chat.id, photo)
    except Exception as e:
        print e
        bot.send_message(message.chat.id, "Can't get image")

@bot.message_handler(commands=["weather"])
def command_weather(message):
    #print message
    bot.send_message(message.chat.id, weather.get_weather(message), parse_mode="HTML")


#start bot polling
#:param none_stop: Do not stop polling when an ApiException occurs.
#:param timeout: Timeout in seconds for long polling.
bot.polling(none_stop=True, timeout=100)
