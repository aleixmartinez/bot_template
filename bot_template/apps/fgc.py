# -*- coding: utf-8 -*-
import requests
import re
from datetime import datetime
#datetime.now().strftime('%Y-%m-%d %H:%M:%S')
class FGC (object):

    def __init__(self):
        self.stations = {
            #S1
            u'NA':[u"Nacions Unides, Terrassa",[u'nacions',u'unides',u'nac']],
            u'EN':[u"Estacio del Nord, Terrassa",[u'estacionord',u'nord',u'nor']],
            u'VP':[u"Vallparadís Universitat, Terrassa",[u'vallparadís',u'vallparadis',u'val']],
            u'TR':[u"Terrassa Rambla",[u'terrassa',u'ter']],
            u'FN':[u"Les Fonts",[u"fonts",u"fon"]],
            u'RB':[u"Rubí",[u'rubí',u'rubi',u'rub']],
            u'HG':[u"Hospital General",[u"hospital",u"general",u"hos"]],
            u'MS':[u"Mira-sol",[u"mira",u"sol",u"mirasol",u"mir"]],
            #S2
            u'RA':[u"Sabadell Rambla",[u'sabadell',u'ram','sab']],
            u'SB':[u"Sabadell Estació",[u'sabadellestació',u'sabadellestacio',u'estacio',u'est']],
            u'SQ':[u"Sant Quirze",[u"santquirze",u"quirze",u"qui"]],
            u'UN':[u"UAB Bellatera",[u'universistat',u'uni',u'uab']],
            u'BT':[u"Bellaterra",[u"bellaterra",u"bella",u"bel"]],
            u'SJ':[u"Sant Joan",[u'santjoan',u'joan',u'joa']],
            u'VO':[u"Volpelleres",[u"volpelleres",u"vol"]],
            #L6
            u'RE':[u"Reina Elisenda",[u"reinaelisenda",u"reina",u"elisenda",u"rei"]],
            #L7
            u'TB':[u"Av. Tibidabo",[u"avtibidabo",u"tibidabo",u"tib"]],
            u'EP':[u"El Putxet",[u"putxet",u"put"]],
            u'PD':[u'Pàdua',[u"pàdua",u"padua",u"pad"]],
            u'PM':[u'Plaça Molina',[u"molina",u"mol"]],
            #COMU
            u'SC':[u"Sant Cugat",[u'santcugat',u'cugat',u'cug']],
            u'VD':[u"Valldoreix",[u"valldoreix",u"vall"]],
            u'LF':[u"La Floresta",[u"floresta",u"flo"]],
            u'LP':[u"Les Planes",[u"planes","pla"]],
            u'VL':[u"Baixador de Vallvidrera",[u"baixador",u"vallvidrera",u"bai"]],
            u'PF':[u"Peu del Funicular",[u"peu",u"funicular",u"fun"]],
            u'SR':[u"Sarrià",["sarria",u"sarrià",u"sar"]],
            u'TT':[u"Les Tres Torres",[u"tres",u"torres",u"tre"]],
            u'BN':[u"La Bonanova",[u"bonanova",u"bon"]],
            u'MN':[u"Muntaner",[u"muntaner",u"mun"]],
            u'SG':[u"Sant Gervasi",[u"santgervasi",u"gervasi",u"ger"]],
            u'GR':[u"Gràcia, BCN",[u'gracia',u'gra']],
            u'PR':[u"Provença, BCN", [u'provença',u'provenca',u'pro']],
            u'PC':[u"Plaça Catalunya, BCN", [u'plaça',u'bcn',u'placa',u'catalunya',u'cat']],
        }
        self.codes_error = {
            u'Wrong_station':u'Invalid Station: ',
            u'Same_stations':u'Same stations!!!!',
            u'Generic_message':u"Something went wrong",
            u'Invalid_code':u"XXX",
        }
        
    def get_code_station(self,station):
        for item in self.stations:
            if station in self.stations[item][1]:
                return item
        return self.codes_error[u'Invalid_code']

    def message_help(self, full_help = False):
        if full_help:
            output = u"Stations:".encode('utf-8')
            for item in self.stations:
                output += unicode(self.stations[item][0]) +u'-->'
                for abbr in self.stations[item][1]:
                    output += u'<b> ' + unicode(abbr) + u'</b>'
                output +=u"\n"
            output +=u"\nusage: origin destiny [hour] [minut]\nex: /fgc terrassa bcn"
            return output
        else:
            return u"\nFor detailed help type <b>/fgc -help</b>\nusage: origin destiny [hour] [minut]\nex: <b>/fgc terrassa bcn</b>"

    def get_schedule(self,origin,destiny,hour,minute):
        line = 1
        url =   "https://www.fgc.cat/cercador/cerca.asp?"
        url +=  "liniasel="+str(line)
        url +=  "&estacio_origen="+str(origin)
        url +=  "&estacio_desti="+str(destiny)
        url +=  "&tipus=S&"
        url +=  "dia="+datetime.now().strftime('%d')
        url +=  "%2F"+datetime.now().strftime('%m')
        url +=  "%2F"+datetime.now().strftime('%Y')
        url +=  "&horas="+str(hour)
        url +=  "&minutos="+str(minute)

        
        pattern = 'sortida":"0[1|2]/01/1900.(.+?):00",(?:.*\]\},\{.*)?"arribada":"0[1|2]/01/1900.(.+?):00".*estacions":\["(..)",'
        query = requests.post(url)
        text = query.text.replace("],[", "]\n[")
        text_list = text.split("\n")

        results = []
        for item in text_list:
            results.append(re.findall(pattern, item))

        for i in range(len(results)):
            if results[i][0][0].startswith("0:"):
                results[i][0] = (results[i][0][0].replace("0:","24:"), results[i][0][1], results[i][0][2])
            if results[i][0][0].startswith("1:"):
                results[i][0] = (results[i][0][0].replace("1:","25:"), results[i][0][1], results[i][0][2])
            if results[i][0][0].startswith("2:"):
                results[i][0] = (results[i][0][0].replace("2:","26:"), results[i][0][1], results[i][0][2])
        for i in range(len(results)):
            if results[i][0][0][1] == ":":
                results[i][0] = ("0" + results[i][0][0], results[i][0][1], results[i][0][2])
        results_sorted=sorted(results)

        try:
            string_header = "Schedule from : "+self.stations[origin][0]+ " to "+ self.stations[destiny][0]
            string_transfer = ""
            output = u""
            
            hour_adapted = hour
            if hour_adapted <="03":
                hour_adapted = str( int(hour_adapted) +24)

            for i in results_sorted:
                if u"NO" != i[0][2]:
                    if  str(i[0][0]) >= hour_adapted+":"+minute:
                        if origin != i[0][2]:
                            string_transfer = u" taking a transfer on: " + self.stations[i[0][2]][0]

                        #clean hours 24/25/26
                        clean = i[0][0]
                        clean = clean.replace("26:","2:").replace("25:","1:").replace("24:","0:")
                        output += u"\nStart: <b>" + unicode(clean) + u"</b> finish: <b>" +  unicode(i[0][1]) + u'</b>'

            return string_header + string_transfer + output
        except Exception as e:
            print e
            return u"Something went wrong"

    def get_params(self,message):
        received = message.text.replace(","," ").split()
        if "help" in message.text.lower():
            return ["HELP_FULL", "", "", ""]
        if len(received) == 1: #/fgc
            return ["HELP", "", "", ""]
        elif len(received) == 2: #/fgc origin
            return [received[1], "bcn", datetime.now().strftime('%H'), datetime.now().strftime('%M')]
        elif len(received) == 3: #/fgc origin destiny
            return [received[1], received[2], datetime.now().strftime('%H'), datetime.now().strftime('%M')]  
        elif len(received) == 4: #/fgc origin destiny hour
            return [received[1], received[2], received[3], "00"]
        elif len(received) == 5: #/fgc origin destiny hour minute
            return [received[1], received[2], received[3], received[4]]
        else:
            return ["HELP", "", "", ""]
                

        return ["HELP", "", "", ""]

    def validate_params(self,param_list):
        try:
            param_list_validated=["","","",""]
            #param Origin
            if not "HELP" in param_list[0]:
                city_code = self.get_code_station(param_list[0])
                if city_code == self.codes_error[u'Invalid_code']:
                    return self.codes_error[u'Wrong_station'] + unicode(param_list[0]) + self.message_help(), False
                else:
                    param_list_validated[0] = city_code
                
                #param destiny
                city_code = self.get_code_station(param_list[1])
                if city_code == self.codes_error[u'Invalid_code']:
                    return self.codes_error[u'Wrong_station'] + unicode(param_list[1]) + self.message_help(), False
                else:
                    param_list_validated[1] = city_code

                if param_list_validated[1] == param_list_validated[0]:
                    return self.codes_error[u'Same_stations'] + self.message_help(), False

                hour = int(param_list[2])%24
                #if hour <=2:
                #    hour +=24
                hour = str(hour)
                if len(hour) == 1:
                    hour = "0" + hour
                param_list_validated[2] = hour

                minute = int(param_list[3])%60
                minute = str(minute)
                if len(minute) == 1:
                    minute = "0" + minute
                param_list_validated[3] = minute

                return param_list_validated, True
            else:
                if "HELP_FULL" == param_list[0]:
                   return self.message_help(True), False
                else:
                    return self.message_help(), False
                return "", False

        except Exception as e:
            print e
            return self.codes_error[u'Generic_message'], False
    def get_trains(self,message):
        output = u''
        try:
            params = self.get_params(message)
            params, validated_params = self.validate_params(params)
            
            if validated_params is True:
                output = self.get_schedule(params[0],params[1],params[2],params[3])
            else:
                output = params + output
        except Exception as e:
            print e
            output = self.codes_error[u'Generic_message'] + self.message_help()

        return output

if __name__ == "__main__":
    fgc = FGC()
    #print fgc.get_trains("/fgc ter pro 22 00")
    #print fgc.get_trains("/fgc rub bel 22 00")
    print fgc.get_trains("/fgc mun ter 0")
    

