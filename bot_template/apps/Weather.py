# -*- coding: utf-8 -*-
import requests

class Weather (object):

    def __init__(self, token):
        self.cities = {
            u'6356055':[u"Barcelona",[u'bcn',u'barcelona',u'bar']],
            u'6356298':[u"Terrassa",[u'ter',u'terrassa']],
            u'6356298':[u"Rubí",[u'rub',u'rubi',u'rubí']],
        }
        self.codes_error = {
            u'Generic_message':u"Something went wrong",
        }

        self.token = token


    def get_city(self, city_name):
        for city in self.cities:
            if city.lower() in self.cities[city][1]:
                return city[0]
        return -1

    def make_response(self, json):
        response = u"City: <b>{0}</b> Country: <b>{1}</b>\nTemp: <b>{2}</b> ºC".format(json['name'], json['sys']['country'], json['main']['temp'])
        return response

    def get_weather(self, message):
        try:
            city = message.text.split(" ")[1:]
        except:
            city = message.split(" ")[1:]

        city_id = self.get_city(city)
        if city_id == -1:
            payload = {'q': city }
        else:
            payload = {'id': city_id}

        payload['units'] = "metric"
        payload['appid'] = self.token
        

        url = "http://api.openweathermap.org/data/2.5/weather"
        r = requests.get(url, params=payload).json()

        return self.make_response(r)



if __name__ == "__main__":
    weather = Weather("8a723824862462695a06918cf0c0c4c8")
    #print weather.get_weather("/weather barcelona")        