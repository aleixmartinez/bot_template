# -*- encoding: utf-8 -*-
import random
import time
import re
import requests
from datetime import datetime
from config import *

def strTimeProp(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def randomDate(start, end, prop):
    return strTimeProp(start, end, '%Y-%m-%d' , prop)

def get_image():

    day =  datetime.now().strftime('%d')
    month =  datetime.now().strftime('%m')
    year =  datetime.now().strftime('%Y')
    today= year+"-"+month+"-"+day
    date_random = randomDate("1989-04-16", today, random.random())

    url_gilbert =requests.get("http://dilbert.com/strip/" + date_random)
    pattern = 'assets.amuniversal.com/(.*?)"'
    image_path = re.findall(pattern, url_gilbert.text)


    #urllib.urlretrieve("http://assets.amuniversal.com/"+image_path[0], "media/images/gilbert.jpg")

    r = requests.get("http://assets.amuniversal.com/"+image_path[0])
    with open(os.path.join(MEDIA_PATH,"img","gilbert","gilbert.jpg"), "wb") as code:
        code.write(r.content)
    code.close()
    return date_random