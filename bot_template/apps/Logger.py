# -*- coding: utf-8 -*-
import logging

class Logger(object):
    def __init__(self,level, name = "error.log", path = ""):
        #DEBUG < INFO < WARNING < ERROR < CRITICAL
        self.level = logging.DEBUG
        if level in [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]:
            self.level = level
        self.path = path
        self.name = name

        logging.basicConfig(filename=self.path + self.name, format='[%(asctime)s] [%(levelname)s] %(message)s', level=self.level, datefmt='%Y-%m-%d %H:%M:%S')

    def write_debug(self,debug):
        logging.debug(debug)

    def write_info(self,info):
        logging.info(info)

    def write_warning(self,warning):
        logging.warning(warning)

    def write_error(self,error):
        logging.error(error)

    def write_critical(self,critical):
        logging.critical(critical)

    def do_something(self):
        self.write_debug("This is a debug log")
        self.write_info("This is an info log")
        self.write_warning("This is a warning log")
        self.write_error("This is an error log")
        self.write_critical("This is a critical log")



if __name__ == "__main__":
    Logger(logging.INFO,"error.log").do_something()
    #print weather.get_weather("/weather barcelona")     